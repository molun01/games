package com.molun.snake;

import javax.swing.*;
import java.net.URL;

public class Data {
    //头部图片
    public static final ImageIcon HEADER = new ImageIcon(getURL("/statics/header.png"));
    //头部朝向：上下左右
    public static final ImageIcon UP = new ImageIcon(getURL("/statics/up.png"));
    public static final ImageIcon DOWN = new ImageIcon(getURL("/statics/down.png"));
    public static final ImageIcon LEFT = new ImageIcon(getURL("/statics/left.png"));
    public static final ImageIcon RIGHT = new ImageIcon(getURL("/statics/right.png"));
    //身体
    public static final ImageIcon BODY = new ImageIcon(getURL("/statics/body.png"));
    //食物
    public static final ImageIcon FOOD = new ImageIcon(getURL("/statics/food.png"));

    //获取URL
    public static URL getURL(String s){
        return Data.class.getResource(s);
    }
}

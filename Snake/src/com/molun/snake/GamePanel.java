package com.molun.snake;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

public class GamePanel extends JPanel implements KeyListener, ActionListener {
	enum Toward {UP, DOWN, LEFT, RIGHT}

	int length; //蛇的长度
	Point[] snake = new Point[400]; //蛇的坐标,假定蛇最长400
	Toward toward = Toward.RIGHT; //蛇的方向
	Timer timer = new Timer(100, this); //定时器：第一个参数，就是定时执行时间
	//食物
	Point food = new Point();
	Random random = new Random();
	boolean isStart = false; //游戏是否开始
	boolean isFail = false; //游戏是否结束
	int score; //游戏分数！

	public GamePanel() {
		init();
		setFocusable(true);
		addKeyListener(this);
	}

	public void init() {
		//初始小蛇有三节,包括小脑袋
		length = 3;
		//初始化开始的蛇,给蛇定位,
		snake[0]=new Point(100, 100);
		snake[1]=new Point(75, 100);
		snake[2]=new Point(50, 100);
		//初始化蛇的移动方向，只在撞墙失败的设定下起作用
		toward = Toward.RIGHT;
		//随机生成一个食物
		produceFood();
		score = 0; //初始化游戏分数
		timer.start();
	}

	public void produceFood() {
		food.setLocation(25 + 25 * random.nextInt(34),
				75 + 25 * random.nextInt(24));
		for (int i = 0; i < length; i++) {
			if (food.equals(snake[i])) {
				produceFood();
				return;
			}
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);//清屏
		this.setBackground(Color.WHITE); //设置面板的背景色
		Data.HEADER.paintIcon(this, g, 25, 11); //绘制头部信息区域
		g.fillRect(25, 75, 850, 600); //绘制游戏区域

		//画蛇头，根据朝向
		switch (toward) {
			case UP:
				Data.UP.paintIcon(this, g, snake[0].x, snake[0].y);
				break;
			case DOWN:
				Data.DOWN.paintIcon(this, g, snake[0].x, snake[0].y);
				break;
			case LEFT:
				Data.LEFT.paintIcon(this, g, snake[0].x, snake[0].y);
				break;
			case RIGHT:
				Data.RIGHT.paintIcon(this, g, snake[0].x, snake[0].y);
				break;
		}
		//画蛇的身体，长度为length-1
		for (int i = 1; i < length; i++) {
			Data.BODY.paintIcon(this, g, snake[i].x, snake[i].y);
		}

		//画食物
		Data.FOOD.paintIcon(this, g, food.x, food.y);

		//游戏分数和长度
		g.setColor(Color.WHITE);
		g.setFont(new Font("微软雅黑", Font.BOLD, 18));
		g.drawString("长度 " + length, 750, 35);
		g.drawString("分数 " + score, 750, 50);

		//游戏开始提示
		g.setFont(new Font("微软雅黑", Font.BOLD, 40));
		if (!isStart) {
			g.drawString("按下空格开始游戏!", 300, 300);
		}
		//失败判断
		if (isFail) {
			g.setColor(Color.RED);
			g.drawString("失败, 按下空格重新开始", 200, 300);
		}

	}

	@Override
	public void actionPerformed(ActionEvent actionEvent) {
		//如果游戏处于开始状态，并且没有结束，则小蛇可以移动
		if (isStart && !isFail) {
			//移动身体：即让身体移动到前一部分身体或头的位置!
			for (int i = length - 1; i > 0; i--) { //除了脑袋都往前移：身体移动
				snake[i].setLocation(snake[i - 1]); //即第i节(后一节)的位置变为(i-1：前一节)节的位置！
			}
			//通过方向控制，头部移动
			//加入设定撞墙失败
			switch (toward) {
				case UP:
					snake[0].translate(0, -25);
					if (snake[0].y < 75) {
//						snake[0].move(snake[0].x, 650); //如果允许穿墙，只需要这一句
						isFail = true;
						repaint();
						timer.stop();
						return;
					}
					break;
				case DOWN:
					snake[0].translate(0, 25);
					if (snake[0].y > 650) {
//						snake[0].move(snake[0].x, 75);
						isFail = true;
						repaint();
						timer.stop();
						return;
					}
					break;
				case LEFT:
					snake[0].translate(-25, 0);
					if (snake[0].x < 25) {
//						snake[0].move(850, snake[0].y);
						isFail = true;
						repaint();
						timer.stop();
						return;
					}
					break;
				case RIGHT:
					snake[0].translate(25, 0);
					if (snake[0].x > 850) {
//						snake[0].move(25, snake[0].y);
						isFail = true;
						repaint();
						timer.stop();
						return;
					}
					break;
			}

			//吃食物:当蛇的头和食物坐标一样时,就算吃到食物!
			if (food.equals(snake[0])) {
				//1.蛇尾先生长一节，然后长度加一
				snake[length]=new Point(snake[length - 1]);
				length++; //这里应该判断一下蛇的最大长度，避免数组越界
				//2.每吃一个食物，增加积分
				score += 10;
				//3.重新生成食物
				produceFood();
			}

			//结束判断，头和身体撞到了(坐标一致)
			for (int i = 1; i < length; i++) {
				if (snake[i].equals(snake[0])) {
					isFail = true;
					timer.stop();
				}
			}
			repaint(); //需要不断的更新页面实现动画
		}
	}

	@Override
	public void keyTyped(KeyEvent keyEvent) {

	}

	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
			case KeyEvent.VK_SPACE: //空格
				if (isFail) { //如果游戏失败,从头再来！
					isFail = false;
					init(); //重新初始化
				} else { //否则，开始或暂停游戏
					isStart = !isStart;
				}
				repaint();
				break;
			//不能反方向移动
			case KeyEvent.VK_UP:
				if (toward != Toward.DOWN) {
					toward = Toward.UP;
				}
				break;
			case KeyEvent.VK_DOWN:
				if (toward != Toward.UP) {
					toward = Toward.DOWN;
				}
				break;
			case KeyEvent.VK_LEFT:
				if (toward != Toward.RIGHT) {
					toward = Toward.LEFT;
				}
				break;
			case KeyEvent.VK_RIGHT:
				if (toward != Toward.LEFT) {
					toward = Toward.RIGHT;
				}
				break;
		}
	}

	@Override
	public void keyReleased(KeyEvent keyEvent) {

	}
}
